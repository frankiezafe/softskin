/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   skinanchor.h
 * Author: frankiezafe
 *
 * Created on May 6, 2019, 2:04 PM
 */

#ifndef SOFTSKINANCHOR_H
#define SOFTSKINANCHOR_H

#include <iostream>
#include <ostream>
#include <vector>

#include "core/math/vector3.h"
#include "scene/3d/spatial.h"

#include "vector_ptr.h"
#include "softskin_common.h"

class SkinAnchor {

public:

    SkinAnchor();
    
    virtual ~SkinAnchor();

    void init(const Vector3& v3, Spatial* skin_root);
    
    void init(const float& x, const float& y, const float& z, Spatial* skin_root);

    void purge();
    
    void register_anchor(Vector3* anchor);

    const VectorPtr< Vector3 >& anchor() const;

    void anchor(const float& x, const float& y, const float& z);

    void parent( Spatial* p );
    
    const Vector3& update(const float& delta_time);

    _FORCE_INLINE_ const bool is_initialised() const {
        return _inititalised;
    }

    friend std::ostream &operator<<(
            std::ostream &os,
            SkinAnchor const &sd
            ) {
        return os <<
                sd.anchor()[0] << ", " <<
                sd.anchor()[1] << ", " <<
                sd.anchor()[2];
    }

private:
    
    Spatial* _skin_root;
    Spatial* _parent;

    Vector3 _anchor_origin;

    VectorPtr< Vector3 > _anchor;

//    SkinDotMirror<VectorPtr< Vector3 >, Vector3> mirror_anchors;

    bool _inititalised;

    void init_internal();

};

#endif /* SOFTSKINANCHOR_H */

