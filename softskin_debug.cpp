/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   skin_debug.cpp
 * Author: frankiezafe
 * 
 * Created on July 18, 2019, 6:44 PM
 */

#include "softskin_debug.h"

SoftskinDebug::SoftskinDebug() {

    _imm = VS::get_singleton()->immediate_create();
    generate_material();
    VS::get_singleton()->immediate_set_material(_imm, _mat->get_rid());
    set_base(_imm);

}

SoftskinDebug::~SoftskinDebug() {
}

void SoftskinDebug::generate_material() {
    _mat = Ref<SpatialMaterial>(memnew(SpatialMaterial));
    _mat->set_flag(SpatialMaterial::FLAG_UNSHADED, true);
    _mat->set_flag(SpatialMaterial::FLAG_ALBEDO_FROM_VERTEX_COLOR, false);
    _mat->set_line_width(SS_DEBUG_LINE_WIDTH);
    _mat->set_albedo(Color(1.0, 1.0, 1.0));
    _own_mat = true;
}

Ref<Material> SoftskinDebug::get_material() {
    return _mat;
}

void SoftskinDebug::set_material(Ref<Material> src) {
    _mat = src;
    _own_mat = false;
    if ( _mat.is_null() ) {
        generate_material();
    }
    VS::get_singleton()->immediate_set_material(_imm, _mat->get_rid());
}

void SoftskinDebug::albedo(const Color& c) {
    if (!_own_mat) return;
    _mat->set_albedo(c);
}

void SoftskinDebug::vertex_color(const bool& enable) {
    if (!_own_mat) return;
    _mat->set_flag(SpatialMaterial::FLAG_ALBEDO_FROM_VERTEX_COLOR, enable);
}

void SoftskinDebug::unshaded(const bool& enable) {
    if (!_own_mat) return;
    _mat->set_flag(SpatialMaterial::FLAG_UNSHADED, enable);
}

void SoftskinDebug::line_width(const float& w) {
    if (!_own_mat) return;
    _mat->set_line_width(w);
}

void SoftskinDebug::start() {
    VS::get_singleton()->immediate_clear(_imm);
    VS::get_singleton()->immediate_begin(_imm, VS::PRIMITIVE_LINES, RID());
}

void SoftskinDebug::push(const Vector3& v0, const Vector3& v1, const Color* c0, const Color* c1) {
    if (c0 != 0 && c1 != 0) {
        VS::get_singleton()->immediate_color(_imm, *c0);
    }
    VS::get_singleton()->immediate_vertex(_imm, v0);
    if (c0 != 0 && c1 != 0) {
        VS::get_singleton()->immediate_color(_imm, *c1);
    }
    VS::get_singleton()->immediate_vertex(_imm, v1);
}

void SoftskinDebug::end() {
    VS::get_singleton()->immediate_end(_imm);
}