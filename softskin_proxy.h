/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   skin_proxy.h
 * Author: frankiezafe
 *
 * Created on July 15, 2019, 6:04 PM
 */

#ifndef SOFTSKIN_PROXY_H
#define SOFTSKIN_PROXY_H

#include <sstream>
#include <map>
#include <algorithm>

#include "softskin_common.h"
#include "softskin_cache.h"
#include "softskin_mesh.h"

class SoftskinProxy : public Resource {
    GDCLASS(SoftskinProxy, Resource);
    OBJ_CATEGORY("Softskin");

public:

    SoftskinProxy();
    virtual ~SoftskinProxy();

    Ref<SoftskinCache> get_cache();
    void set_cache(Ref<SoftskinCache>);

    Ref<Mesh> get_mesh();
    void set_mesh(Ref<Mesh>);
    
    Ref<SoftskinMesh> get_skin();
    void set_skin(Ref<SoftskinMesh>);
    
    _FORCE_INLINE_ bool is_ready() const {
        return (!_cache.is_null() && _cache->is_processed());
    }

protected:

    static void _bind_methods();

    void _notification(int p_what);

    void mesh_changed();
    
    void cache_changed();
    
    void skin_changed();

    bool analyse_mesh();
    
    bool analyse_skin();

    int vector3_find(const Vector3& p, int surfid);

    void analyse_cache();

private:

    Ref<SoftskinCache> _cache;
    Ref<SoftskinMesh> _skin;
    Ref<Mesh> _mesh;

};

#endif /* SOFTSKIN_PROXY_H */