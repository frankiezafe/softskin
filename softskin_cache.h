/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   skin_cache.h
 * Author: frankiezafe
 *
 * Created on July 16, 2019, 1:19 PM
 */

#ifndef SOFTSKIN_CACHE_H
#define SOFTSKIN_CACHE_H

#include "softskin_resource.h"

class SoftskinCache : public SkinResource {
    GDCLASS(SoftskinCache, SkinResource);
    OBJ_CATEGORY("Softskin");

public:

    struct MeshArray {
        uint32_t surface_format;
        uint32_t primitive_type;
        uint32_t stride;
        uint32_t offset_vertices;
        uint32_t offset_normal;
        uint32_t offset_uv;
        // buffer: ULTRA IMPORTANT, contains the mesh has it is in GPU!
        PoolVector<uint8_t> buffer;
        PoolVector<Vector3> verts;
        PoolVector<Vector3> normals;
        PoolVector<int> indices;
        Array arr;
    };
    
    // see servers/visual_server.cpp, _surface_set_data for types

    struct array_decompressor { // ArrayType ~
        PoolVector<Vector3> ARRAY_VERTEX;   // 0
        PoolVector<Vector3> ARRAY_NORMAL;   // 1
        PoolVector<real_t> ARRAY_TANGENT;   // 2
        PoolVector<Color> ARRAY_COLOR;      // 3
        PoolVector<Vector2> ARRAY_TEX_UV;   // 4
        PoolVector<Vector2> ARRAY_TEX_UV2;  // 5
        PoolVector<int> ARRAY_BONES;        // 6
        PoolVector<real_t> ARRAY_WEIGHTS;   // 7
        PoolVector<int> ARRAY_INDEX;        // 8
    };

    // analysis
    ss_unique_map _all2unique;
    std::vector<v3_ref> _unique_dots;
    std::vector<v3_ref> _unique_anchors;
    std::vector<edge_ref> _unique_fibers;
    std::vector<edge_ref> _unique_ligaments;
    std::vector< normal_ref > _unique_normals;
    
    // TO ADD IN SERIALISATION!
    PoolVector<Vector3> _anchors;
    std::vector<group_ref> _groups;
    
    // no need to save, temporary data! 
    ss_face_map _face_all_verts;
    ss_face_map _face_fibers;

    // mesh surfaces data
    std::vector<MeshArray*> _surfaces;

    SoftskinCache();
    virtual ~SoftskinCache();

    void purge();

    void purge_analysis();

    Error save_file(const String &p_path);

    Error load_file(const String &p_path);

    void load(Ref<Mesh>);

    bool compare(const SoftskinCache* src);
    
    void copy_bufferinfo( SoftskinData* dst, const size_t& surfID );

    _FORCE_INLINE_ bool is_loaded() const {
        return !_surfaces.empty();
    }

    _FORCE_INLINE_ bool is_processed() const {
        return is_loaded() &&
                !_all2unique.empty();
    }

    void dump();
    
    static void decompressor_to_array( const array_decompressor&, Array& );
    
    static void array_to_decompressor( const Array&, array_decompressor& );
    
    static void dump_arr( const Array& a );

protected:

    static void _bind_methods();
    
    void serialise(FileAccess* f);
    bool deserialise(FileAccess* f);
    
    void save_surfaces(FileAccess* f);
    void save_all2unique(FileAccess* f);
    void save_unique_dots(FileAccess* f);
    void save_unique_anchors(FileAccess* f);
    void save_unique_fibers(FileAccess* f);
    void save_unique_ligaments(FileAccess* f);
    void save_face_map(FileAccess* f, ss_face_map&);
    void save_unique_normals(FileAccess* f);
    void save_anchors(FileAccess* f);
    void save_groups(FileAccess* f);
    void save_array(FileAccess* f, Array&);
    
    bool load_surfaces(FileAccess* f);
    bool load_all2unique(FileAccess* f);
    bool load_unique_dots(FileAccess* f);
    bool load_unique_anchors(FileAccess* f);
    bool load_unique_fibers(FileAccess* f);
    bool load_unique_ligaments(FileAccess* f);
    bool load_face_map(FileAccess* f, ss_face_map&);
    bool load_unique_normals(FileAccess* f);
    bool load_anchors(FileAccess* f);
    bool load_groups(FileAccess* f);
    bool load_array(FileAccess* f, Array&);

};

#endif /* SOFTSKIN_CACHE_H */

