/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   skin_normal.h
 * Author: frankiezafe
 *
 * Created on May 31, 2019, 4:45 PM
 */

#ifndef SOFTSKIN_NORMAL_H
#define SOFTSKIN_NORMAL_H

#include "core/vector.h"

#include "softskin_dot.h"
#include "softskin_edge.h"

class SoftskinNormal {

    struct fdata {
        SoftskinDot* dot;
        SoftskinEdge* edge0;
        SoftskinEdge* edge1;
        int flip0;
        int flip1;
    };

public:

    SoftskinNormal();
    
    virtual ~SoftskinNormal();

    void push_back(SoftskinDot* d, SoftskinEdge* e0, SoftskinEdge* e1, bool flip0, bool flip1);

    void update();
    
    const Vector3& get_normal() const;
    
    const real_t* coord() const;
    
    void operator = ( const Vector3& src ) {
        _normal = src;
    }
    
    _FORCE_INLINE_ int get_face_size() const {
        return _faces.size();
    }
    
    _FORCE_INLINE_ const SoftskinDot* dot(const int i) const {
        return _faces[i].dot;
    }

private:

    Vector3 _normal;
    PoolVector<fdata> _faces;
    
    bool _ready;

};

#endif /* SOFTSKIN_NORMAL_H */

