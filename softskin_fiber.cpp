/*
 * 
 * 
 * _________ ____  .-. _________/ ____ .-. ____ 
 * __|__  (_)_/(_)(   )____<    \|    (   )  (_)
 *                 `-'                 `-'      
 * 
 * 
 * art & game engine
 * 
 * ____________________________________  ?   ____________________________________
 *                                     (._.)
 * 
 * 
 * This file is part of softskin library
 * For the latest info, see http://polymorph.cool/
 * 
 * Copyright (c) 2018 polymorph.cool
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 * ___________________________________( ^3^)_____________________________________
 * 
 * ascii font: rotated by MikeChat & myflix
 * have fun and be cool :)
 * 
 */

#include "softskin_fiber.h"

bool SoftskinFiber::init(SoftskinDot* a, SoftskinDot* b) {
    if (_head != 0 || _tail != 0) {
        std::cout <<
                "SoftskinFiber already initialised!" <<
                std::endl;
        return false;
    }
    _head = a;
    _tail = b;
    const Vector3& av = _head->vert().ref();
    const Vector3& bv = _tail->vert().ref();
    _rest_len = av.distance_to(bv);
    _init_rest_len = _rest_len;
    _dir = ((av)-(bv)).normalized();
    _middle = av + _dir * _rest_len * 0.5;
    _type = sf_FIBER;
    return true;
}

void SoftskinFiber::update_specialised() {
    const Vector3& av = _head->vert().ref();
    _dir = _tail->vert().ref() - av;
    _current_len = _dir.length();
    float dl = _current_len - (_rest_len * _rest_len_multiplier);
    _dir.normalize();
    Vector3 d = _dir * dl * _stiffness;
    d *= 0.5;
    _head->push(d);
    _tail->push(-d);
    _middle = av + _dir * _current_len * 0.5;
}

bool SoftskinLigament::init(SkinAnchor* a, SoftskinDot* b) {
    if (_head != 0 || _tail != 0) {
        std::cout <<
                "SoftskinLigament already initialised!" <<
                std::endl;
        return false;
    }
    _head = a;
    _tail = b;
    const Vector3& av = _head->anchor().ref();
    const Vector3& bv = _tail->vert().ref();
    _rest_len = av.distance_to(bv);
    _init_rest_len = _rest_len;
    _dir = ((av)-(bv)).normalized();
    _middle = av + _dir * _rest_len * 0.5;
    _type = sf_LIGAMENT;
    return true;
}

void SoftskinLigament::parent( Spatial* s ) {
    ERR_FAIL_NULL( _head );
    _head->parent( s );
}

void SoftskinLigament::update_specialised() {
    const Vector3& av = _head->anchor().ref();
    _dir = _tail->vert().ref() - av;
    _current_len = _dir.length();
    float dl = _current_len - (_rest_len * _rest_len_multiplier);
    _dir.normalize();
    _dir *= _stiffness * dl;
    _tail->push(-_dir);
    _force_feedback = _dir;
    _middle = av + _dir * _current_len * 0.5;
}