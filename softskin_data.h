/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   skin_data.h
 * Author: frankiezafe
 *
 * Created on July 15, 2019, 6:55 PM
 */

#ifndef SOFTSKIN_DATA_H
#define SOFTSKIN_DATA_H

#include "softskin_common.h"
#include "softskin_anchor.h"
#include "softskin_normal.h"
#include "softskin_group.h"

class v3_ref {
public:

    bool belong_2_face;
    uint32_t local_id;
    Vector3 xyz;
    Vector<uint32_t> mirrors; // similar indices in _all_vertices

    v3_ref();

    int find_mirror(uint32_t i) const;

    _FORCE_INLINE_ void operator=(const Vector3 & src) {
        xyz = src;
    }

};

class two_edge {
public:
    
    uint32_t buffer_id;
    uint32_t edge0;
    uint32_t edge1;
    bool flip0;
    bool flip1;

    two_edge();

    void swap();

    _FORCE_INLINE_ void operator=(const two_edge& src) {
        buffer_id = src.buffer_id;
        edge0 = src.edge0;
        edge1 = src.edge1;
        flip0 = src.flip0;
        flip1 = src.flip1;
    }

};

class normal_ref {
public:
    
    uint32_t dot_id;
    Vector<uint32_t> faces; // indices of faces using the normal
    std::vector<two_edge> edges; // pair of edges to use face per face
    
    normal_ref();
    
    void push_face(uint32_t i);
    
    _FORCE_INLINE_ void operator=(const normal_ref& src) {
        dot_id = src.dot_id;
        faces = src.faces;
        edges = src.edges;
    }
    
};

struct edge_ref {
    uint32_t v3_ref_0;
    uint32_t v3_ref_1;
    bool ligament;
    Vector<uint32_t> faces;

    edge_ref();

    bool set(uint32_t v0, uint32_t v1);

    int contains(uint32_t vid);

    void push_face(uint32_t i);

    _FORCE_INLINE_ bool operator==(const edge_ref& src) const {
        return v3_ref_0 == src.v3_ref_0 &&
                v3_ref_1 == src.v3_ref_1;
    }
    
    _FORCE_INLINE_ void operator=(const edge_ref& src) {
        v3_ref_0 = src.v3_ref_0;
        v3_ref_1 = src.v3_ref_1;
        ligament = src.ligament;
        faces = src.faces;
    }

};

struct group_ref {
    std::string name;
    softskin_type_t type;
    PoolVector<int32_t> indices;
    PoolVector<float> weights;
};

class SoftskinData {
public:

    uint32_t surface_format;
    uint32_t primitive_type;
    uint32_t stride;
    uint32_t offset_vertices;
    uint32_t offset_normal;
    uint32_t offset_uv;

    uint32_t geom_buffer_num;
    uint32_t verts_num;
    uint32_t normals_num;
    uint32_t forces_num;
    uint32_t dots_num;
    uint32_t anchors_num;
    uint32_t fibers_num;
    uint32_t ligaments_num;
    uint32_t groups_num;

    const real_t** vert_buffer; // same size as geom_buffer_num!
    const real_t** norm_buffer; // same size as geom_buffer_num!
    Vector3* verts;
    Vector3* forces;
    SoftskinDot* dots;
    SkinAnchor* anchors;
    SoftskinFiber* fibers;
    SoftskinLigament* ligaments;
    SoftskinNormal* normals;
    SoftskinGroup** groups;

    SoftskinData();
    ~SoftskinData();

    void purge_anchors();

    void purge_ligaments();

    void purge();

    void validate();

    const bool& is_valid() const;

protected:

    bool _valid;

};

#endif /* SOFTSKIN_DATA_H */
