/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   skin_resource.cpp
 * Author: frankiezafe
 * 
 * Created on July 21, 2019, 12:22 AM
 */

#include "softskin_resource.h"

SkinResource::SkinResource() {
}

SkinResource::~SkinResource() {
}

void SkinResource::_bind_methods() {
}

bool SkinResource::compare(PoolVector<Vector3>& local, const PoolVector<Vector3>& foreign) {
    if (local.size() != foreign.size()) {
        return false;
    }
    for (uint32_t j = 0, jmax = uint32_t(local.size()); j < jmax; ++j) {
        if (local[j] != foreign[j]) {
            return false;
        }
    }
    return true;
}

bool SkinResource::compare(PoolVector<float>& local, const PoolVector<float>& foreign) {
    if (local.size() != foreign.size()) {
        return false;
    }
    for (uint32_t j = 0, jmax = uint32_t(local.size()); j < jmax; ++j) {
        if (local[j] != foreign[j]) {
            return false;
        }
    }
    return true;
}

bool SkinResource::compare(PoolVector<int>& local, const PoolVector<int>& foreign) {
    if (local.size() != foreign.size()) {
        return false;
    }
    for (uint32_t j = 0, jmax = uint32_t(local.size()); j < jmax; ++j) {
        if (local[j] != foreign[j]) {
            return false;
        }
    }
    return true;
}

void SkinResource::save_marker(FileAccess* f, const char* s) {
    memcpy(st10u.v, s, SSC_MARKER_SIZE);
    f->store_buffer(st10u.bytes, SSC_MARKER_SIZE);
}

void SkinResource::load_marker(FileAccess* f, std::string& s) {
    f->get_buffer(st10u.bytes, SSC_MARKER_SIZE);
    s = st10u.v;
}

void SkinResource::save_value(FileAccess* f, const char* s) {
    memcpy(st2u.v, s, 255);
    f->store_buffer(st2u.bytes, __ss_str_size);
}

void SkinResource::load_value(FileAccess* f, std::string& s) {
    f->get_buffer(st2u.bytes, __ss_str_size);
    s = st2u.v;
}

void SkinResource::save_value(FileAccess* f, const float& v) {
    f->store_float(v);
}

void SkinResource::load_value(FileAccess* f, float& v) {
    v = f->get_float();
}

void SkinResource::save_value(FileAccess* f, const int& v) {
    assert(v >= 0);
    f->store_32(v);
}

void SkinResource::load_value(FileAccess* f, int& v) {
    v = f->get_32();
}

void SkinResource::save_value(FileAccess* f, const size_t& v) {
    f->store_32(v);
}

void SkinResource::load_value(FileAccess* f, size_t& v) {
    v = f->get_32();
}

void SkinResource::save_value(FileAccess* f, const uint32_t& v) {
    f->store_32(v);
}

void SkinResource::load_value(FileAccess* f, uint32_t& v) {
    v = f->get_32();
}

void SkinResource::save_poolvector(FileAccess* f, PoolVector<uint8_t>& pv) {
    uint8_t* buff = new uint8_t[pv.size()];
    for (uint32_t i = 0, imax = uint32_t(pv.size()); i < imax; ++i) {
        buff[i] = pv[i];
    }
    f->store_buffer(buff, pv.size());
    delete[] buff;
}

void SkinResource::load_poolvector(FileAccess* f, const size_t& num, PoolVector<uint8_t>& pv) {

    if (num == 0) return;
    uint8_t* buff = new uint8_t[num];
    f->get_buffer(buff, num);
    pv.resize(num);
    PoolVector<uint8_t>::Write w = pv.write();
    for (size_t i = 0; i < num; ++i) {
        w[i] = buff[i];
    }
    delete[] buff;
}

void SkinResource::save_poolvector(FileAccess* f, PoolVector<Vector3>& pv) {
    for (uint32_t i = 0, imax = uint32_t(pv.size()); i < imax; ++i) {
        save_value(f, pv[i].x);
        save_value(f, pv[i].y);
        save_value(f, pv[i].z);
    }
}

void SkinResource::load_poolvector(FileAccess* f, const size_t& num, PoolVector<Vector3>& pv) {

    if (num == 0) return;
    pv.resize(num);
    PoolVector<Vector3>::Write w = pv.write();
    for (size_t i = 0; i < num; ++i) {
        load_value(f, w[i].x);
        load_value(f, w[i].y);
        load_value(f, w[i].z);
    }
}

void SkinResource::save_poolvector(FileAccess* f, PoolVector<Vector2>& pv) {
    for (uint32_t i = 0, imax = uint32_t(pv.size()); i < imax; ++i) {
        save_value(f, pv[i].x);
        save_value(f, pv[i].y);
    }
}

void SkinResource::load_poolvector(FileAccess* f, const size_t& num, PoolVector<Vector2>& pv) {

    if (num == 0) return;
    pv.resize(num);
    PoolVector<Vector2>::Write w = pv.write();
    for (size_t i = 0; i < num; ++i) {
        load_value(f, w[i].x);
        load_value(f, w[i].y);
    }
}

void SkinResource::save_poolvector(FileAccess* f, PoolVector<Color>& pv) {
    for (uint32_t i = 0, imax = uint32_t(pv.size()); i < imax; ++i) {
        save_value(f, pv[i].r);
        save_value(f, pv[i].g);
        save_value(f, pv[i].b);
        save_value(f, pv[i].a);
    }
}

void SkinResource::load_poolvector(FileAccess* f, const size_t& num, PoolVector<Color>& pv) {

    if (num == 0) return;
    pv.resize(num);
    PoolVector<Color>::Write w = pv.write();
    for (size_t i = 0; i < num; ++i) {
        load_value(f, w[i].r);
        load_value(f, w[i].g);
        load_value(f, w[i].b);
        load_value(f, w[i].a);
    }
}

void SkinResource::save_poolvector(FileAccess* f, PoolVector<float>& pv) {
    for (uint32_t i = 0, imax = uint32_t(pv.size()); i < imax; ++i) {
        save_value(f, pv[i]);
    }
}

void SkinResource::load_poolvector(FileAccess* f, const size_t& num, PoolVector<float>& pv) {

    if (num == 0) return;
    pv.resize(num);
    PoolVector<float>::Write w = pv.write();
    for (size_t i = 0; i < num; ++i) {
        load_value(f, w[i]);
    }
}

void SkinResource::save_poolvector(FileAccess* f, PoolVector<int>& pv) {
    for (uint32_t i = 0, imax = uint32_t(pv.size()); i < imax; ++i) {
        save_value(f, pv[i]);
    }
}

void SkinResource::load_poolvector(FileAccess* f, const size_t& num, PoolVector<int>& pv) {

    if (num == 0) return;
    pv.resize(num);
    PoolVector<int>::Write w = pv.write();
    for (size_t i = 0; i < num; ++i) {
        load_value(f, w[i]);
    }
}

void SkinResource::save_stdvector(FileAccess* f, ss_unique_vec& v) {
    ss_unique_vec::iterator it = v.begin();
    ss_unique_vec::iterator ite = v.end();
    for (; it != ite; ++it) {
        save_value(f, *it);
    }
}

void SkinResource::load_stdvector(FileAccess* f, const size_t& num, ss_unique_vec& v) {

    if (num == 0) return;
    v.resize(num);
    for (size_t i = 0; i < num; ++i) {
        load_value(f, v[i]);
    }
}

bool SkinResource::is_marker(uint8_t* us, const char* s) {
    uint32_t i = 0;
    while (i < SSC_MARKER_SIZE) {
        if (char(us[i]) != s[i]) {
            return false;
        }
        ++i;
    }
    return true;
}

bool SkinResource::is_marker(FileAccess* f, const char* s) {
    uint8_t* us = new uint8_t[SSC_MARKER_SIZE];
    f->get_buffer(us, SSC_MARKER_SIZE);
    bool b = is_marker(us, s);
    delete[] us;
    return b;
}