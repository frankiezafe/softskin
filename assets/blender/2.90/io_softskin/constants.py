"""
All constant data used in the package should be defined here.
"""

import enum
import mathutils

# BINARY FLAGS
SOFTSKIN_VERSION = 			110
SSM_NAME = 					"softskin mesh"
SSM_EXT = 					"ssm"
SSC_STRING_SIZE = 			255 			#num of char for free strings
SSC_MARKER_SIZE = 			10 			#num of char for marker strings
SSC_SSMSTARTMA = 			"ssmstartma"
SSC_END_MARKER = 			"end_marker"

SSH_GDOT_MARKER = 			"ssd_"
SSH_GANCHOR_MARKER = 		"ssa_"
SSH_GFIBER_MARKER = 		"ssf_"
SSH_GLIGAMENTS_MARKER = 	"ssl_"

class SSType(enum.IntEnum):
	SSUNDEFINED = 			0,
	SSDOT = 				1,
	SSANCHOR = 				2,
	SSEDGE = 				3,
	SSFIBER = 				4,
	SSTENSOR = 				5,
	SSLIGAMENT = 			6,
	SSMUSCLE = 				7

SSMat = mathutils.Matrix()
SSMat[0] = mathutils.Vector((1,0,0,0))
SSMat[1] = mathutils.Vector((0,0,1,0))
SSMat[2] = mathutils.Vector((0,1,0,0))
SSMat[3] = mathutils.Vector((0,0,0,1))
