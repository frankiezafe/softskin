import bpy
import math
import mathutils
import enum
import copy
import struct # C types
import json

from . import constants

class SSGroup():
	
	name = None # name of the group without the prefix
	ss_type = None  # not exported if SSUNDEFINED
	indices = None # list of indices, depending on type
	weights = None # one float per index
	
	def __init__(self):
		self.name = ''
		self.ss_type = constants.SSType.SSUNDEFINED
		self.indices = []
		self.weights = []
	
	def dump(self):
		print('\t\tname', self.name)
		print('\t\t>>> ss_type', self.ss_type)
		print('\t\t>>> indices', len(self.indices)) #) #, '\n', self.indices )
		print('\t\t>>> weights', len(self.weights)) #) #, '\n', self.weights )
	
	def to_json(self):
		return {
			'name': self.name,
			'type': self.ss_type,
			'indices': self.indices,
			'weights': self.weights
		}

class SSVertex():
	
	blender_id = -1
	ss_id = -1
	ss_type = constants.SSType.SSUNDEFINED
	vertex = None
	
	def __init__(self, ss_type, bid, ssid, v):
		self.blender_id = bid
		self.ss_id = ssid
		self.ss_type = ss_type
		self.vertex = v
	
	def dump(self):
		print('\t\t>>> blender_id', self.blender_id)
		print('\t\t>>> ss_id', self.ss_id)
		print('\t\t>>> ss_type', self.ss_type)
		print('\t\t>>> vertex', self.vertex)

class SSMesh():
	
	mesh = None
	
	anchors = None # list of floats, 3x3
	dots = None # list of floats, 3x3
	normals = None # list of floats, 3x3
	tangents = None # list of floats, 3x3
	colors = None # list of floats, 4x4
	uvs = None # list of floats, 2x2
	uv2s = None # list of floats, 2x2
	extra_uvs = None # list of lists of floats, 2x2
	indices = None # list of vert index, 3x3
	face_fibers = None # list of edges in face 3x3
	fibers = None # list of vert index, 2x2
	ligaments = None # list of vert index, 2x2
	groups = None # list of softskin_groups
	unique_normals = None # list of lists: dotID, (bufferID, fiberID, flip, fiberID, flip) * n
	
	last_vertid = 0
	last_anchorid = 0
	tmp_verts = None # list of SSVertex, used to hold self.mesh analysis and differentiate dots from anchors
	tmp_fibers_ids = None # map from blender ids to softskin ids
	tmp_edges = None # list of vert index, 2x2
	tmp_fibers = None # list of vert index, 2x2
	tmp_ligaments = None # list of vert index, 2x2
	tmp_groups = None # dict of lists of vert index, key = group id
	tmp_vert_sharp_count = None
	tmp_unique_edge_pair = None
	tmp_all_normal = None # list of dictionaries: for each normal in buffer: fibers to use, reference to dot & merge boolean
	
	def __init__(self):
		
		self.mesh = None
		
		self.anchors = []
		self.dots = []
		self.normals = []
		self.tangents = []
		self.colors = []
		self.uvs = []
		self.uv2s = []
		self.extra_uvs = []
		self.indices = []
		self.face_fibers = []
		self.fibers = []
		self.ligaments = []
		self.groups = []
		self.unique_normals = []
		
		self.last_vertid = 0
		self.last_anchorid = 0
		
		self.tmp_verts = []
		self.tmp_fibers_ids = {}
		self.tmp_edges = []
		self.tmp_fibers = []
		self.tmp_ligaments = []
		self.tmp_groups = {}
		self.tmp_vert_sharp_count = {}
		self.tmp_unique_edge_pair = {}
		self.tmp_all_normal = []
		
		pass
	
	def dump(self):
		print('\t>>> mesh', self.mesh)
		print('\t>>> anchors', len(self.anchors)) #, '\n', self.anchors)
		print('\t>>> vertex', len(self.dots)) #, '\n', self.dots)
		print('\t>>> normal', len(self.normals)) #, '\n', self.normals)
		print('\t>>> tangent', len(self.tangents)) #, '\n', self.tangents)
		print('\t>>> color', len(self.colors)) #, '\n', self.colors)
		print('\t>>> uv', len(self.uvs)) #, '\n', self.uvs)
		print('\t>>> uv2', len(self.uv2s)) #, '\n', self.uv2s)
		print('\t>>> extra_uv', len(self.extra_uvs)) #, '\n', self.extra_uvs)
		print('\t>>> index', len(self.indices)) #, '\n', self.indices)
		print('\t>>> face_fibers', len(self.face_fibers)) #, '\n', self.face_fibers)
		print('\t>>> fibers', len(self.fibers)) #, '\n', self.fibers)
		print('\t>>> ligaments', len(self.ligaments)) #, '\n', self.ligaments)
		print('\t>>> groups', len(self.groups)) #, '\n', self.groups)
		for g in self.groups:
			g.dump()
		print('\t>>> unique_normals', len(self.unique_normals)) #, '\n', self.groups)
		for ne in self.unique_normals:
			# num of faces using this normal:
			fn = ( len( ne ) - 1 ) / 5
			print('\t\tdot id:', ne[0], 'used in', fn, 'face(s)')
	
	def identify_vertex(self, vid, vert):
		
		if vid in self.indices:
			self.tmp_verts.append(SSVertex(constants.SSType.SSDOT, vid, self.last_vertid, vert))
			self.last_vertid += 1
		elif vid in self.tmp_edges:
			self.tmp_verts.append(SSVertex(constants.SSType.SSANCHOR, vid, self.last_anchorid, vert))
			self.last_anchorid += 1
		else:
			self.tmp_verts.append(SSVertex(constants.SSType.SSUNDEFINED, vid, -1, vert))
	
	def get_vert_group(self, vert, gid):
		
		for g in vert.groups:
			if g.group == gid:
				return g
		return None
	
	def get_group_ids(self, obj, gid):
		
		bpy.ops.object.mode_set(mode = 'EDIT')
		vs = [ v for v in obj.data.vertices if gid in [ vg.group for vg in v.groups ] ]
		vlen = len(vs)
		if (vlen == 0):
			return
		selv = []
		vsindex = 0
		vid = 0
		for v in obj.data.vertices:
			if v == vs[vsindex]:
				selv.append(vid)
				vsindex += 1
			if vsindex >= vlen:
				break
			vid += 1
		bpy.ops.object.mode_set(mode = 'OBJECT')
		self.tmp_groups[gid] = selv
	
	def get_tmp_group(self, gid):
		
		if gid in self.tmp_groups:
			return self.tmp_groups[gid]
		return None
	
	def load_dot_group(self, obj, group, gid):
		
		tmpg = self.get_tmp_group(gid)
		if tmpg == None:
			print('SSMesh::load_dot_group, ' + str(gid) + ' is not in tmp_groups.')
			return
		
		ssg = SSGroup()
		ssg.__init__()
		ssg.name = group.name[len(constants.SSH_GDOT_MARKER):]
		ssg.ss_type = constants.SSType.SSDOT
		
		for vid in tmpg:
			v = self.tmp_verts[vid]
			if v.ss_type != constants.SSType.SSDOT:
				continue
			ssg.indices.append(v.ss_id)
			wfound = False
			for g in v.vertex.groups:
				if g.group == gid:
					wfound = True
					ssg.weights.append(g.weight)
					break
			if not wfound:
				ssg.weights.append(1)
		
		self.groups.append(ssg)
	
	def load_anchor_group(self, obj, group, gid):
		
		tmpg = self.get_tmp_group(gid)
		if tmpg == None:
			print('SSMesh::load_anchor_group, ' + str(gid) + ' is not in tmp_groups.')
			return
		
		ssg = SSGroup()
		ssg.__init__()
		ssg.name = group.name[len(constants.SSH_GANCHOR_MARKER):]
		ssg.ss_type = constants.SSType.SSANCHOR
		
		for vid in tmpg:
			v = self.tmp_verts[vid]
			if v.ss_type != constants.SSType.SSANCHOR:
				continue
			ssg.indices.append(v.ss_id)
			for g in v.vertex.groups:
				if g.group == gid:
					ssg.weights.append(g.weight)
					break
			if not wfound:
				ssg.weights.append(1)
		
		self.groups.append(ssg)
	
	def load_fiber_group(self, obj, group, gid):
		
		tmpg = self.get_tmp_group(gid)
		if tmpg == None:
			print('SSMesh::load_fiber_group, ' + str(gid) + ' is not in tmp_groups.')
			return
		
		ssg = SSGroup()
		ssg.__init__()
		ssg.name = group.name[len(constants.SSH_GFIBER_MARKER):]
		ssg.ss_type = constants.SSType.SSFIBER
		
		for findex in range(0,len(self.fibers),2):
			vid0 = self.tmp_fibers[findex]
			vid1 = self.tmp_fibers[findex+1]
			if vid0 in tmpg and vid1 in tmpg:
				ssg.indices.append(int(findex / 2))
				w = -1
				for g in self.mesh.vertices[vid0].groups:
					if g.group == gid:
						w = g.weight
						break
				for g in self.mesh.vertices[vid1].groups:
					if g.group == gid:
						if g.weight > w:
							w = g.weight
						break
				if w == -1:
					w = 1
				ssg.weights.append(w)
		
		self.groups.append(ssg)
		
	def load_ligament_group(self, obj, group, gid):
		
		tmpg = self.get_tmp_group(gid)
		if tmpg == None:
			print('SSMesh::load_fiber_group, ' + str(gid) + ' is not in tmp_groups.')
			return
		
		ssg = SSGroup()
		ssg.__init__()
		ssg.name = group.name[len(constants.SSH_GLIGAMENTS_MARKER):]
		ssg.ss_type = constants.SSType.SSLIGAMENT
		
		for findex in range(0,len(self.ligaments),2):
			vid0 = self.tmp_ligaments[findex]
			vid1 = self.tmp_ligaments[findex+1]
			keepon = True
			if vid0 in tmpg and vid1 in tmpg:
				ssg.indices.append(int(findex / 2))
				keepon = False
			if keepon:
				v0 = self.tmp_verts[ vid0 ]
				if v0.ss_type == constants.SSType.SSANCHOR:
					if vid0 in tmpg:
						ssg.indices.append(int(findex / 2))
						keepon = False
			if keepon:
				v1 = self.tmp_verts[ vid1 ]
				if v1.ss_type == constants.SSType.SSANCHOR:
					if vid1 in tmpg:
						ssg.indices.append(int(findex / 2))
						keepon = False
			if not keepon: # a solution has been found
				w = -1
				for g in self.mesh.vertices[vid0].groups:
					if g.group == gid:
						w = g.weight
						break
				for g in self.mesh.vertices[vid1].groups:
					if g.group == gid:
						if g.weight > w:
							w = g.weight
						break
				if w == -1:
					w = 1
				ssg.weights.append(w)
		
		self.groups.append(ssg)
	
	def load_norm_edges(self, polygon, v):
		
		vid = v.blender_id
		
		# unique normal ID
		norm_info = {
			'dotid' : v.ss_id, # dot to push normal to
			'bufferid' : len( self.tmp_all_normal ), # position in the geometry buffer
			'edgeids' : [],	# fiber id + flip info
			'merge' : True, # if false, this normal is unique, else, it can be merged with others
			'sharp0' : False,
			'sharp1' : False
		}
		edgeids = norm_info['edgeids']
			
		if not vid in polygon.vertices:
			print( 'SSMesh:load_norm_edges ERROR,', vid, 'not found in this polygon!' )
			return
		
		if len(polygon.vertices) == 3:
		
			# previous & next ids (in reverse order)
			prevv = -1
			nextv = -1
			for i in range(0,3):
				if polygon.vertices[i] == vid:
					prevv = (i + 2) % 3
					nextv = (i + 1) % 3
			prevv = polygon.vertices[prevv]
			nextv = polygon.vertices[nextv]

			prev_i = -1
			prev_e = None
			next_i = -1
			next_e = None

			for i in range(0, polygon.loop_total):
				e = self.mesh.edges[ self.mesh.loops[ polygon.loop_start + i ].edge_index ]
				if vid in e.vertices and prevv in e.vertices:
					prev_i = self.mesh.loops[ polygon.loop_start + i ].edge_index
					prev_e = e
				elif vid in e.vertices and nextv in e.vertices:
					next_i = self.mesh.loops[ polygon.loop_start + i ].edge_index
					next_e = e

			edgeids.append( int(self.tmp_fibers_ids[next_i]) )
			if next_e.vertices[0] == vid:
				edgeids.append(0)
			else:
				edgeids.append(1)

			edgeids.append( int(self.tmp_fibers_ids[prev_i]) )
			if prev_e.vertices[0] == vid:
				edgeids.append(0)
			else:
				edgeids.append(1)
			
			if vid in self.tmp_vert_sharp_count:
				# addding edges until falling on the sharp one
				if next_e.use_edge_sharp:
					norm_info['sharp0'] = True
				if prev_e.use_edge_sharp:
					norm_info['sharp1'] = True
				norm_info['merge'] = False
			
			if not v.ss_id in self.tmp_unique_edge_pair:
				self.tmp_unique_edge_pair[v.ss_id] = []
			self.tmp_unique_edge_pair[v.ss_id].append( norm_info )
		
		self.tmp_all_normal.append(norm_info)
	
	def split_normals(self, unique_pairs ):
		
		# launching the merge process
		dotid = unique_pairs[0]['dotid']
		sharps = []
		
		for ne in unique_pairs:
			
			if not ne['edgeids'][0] in sharps and ne['sharp0']:
				sharps.append( ne['edgeids'][0] )
			if not ne['edgeids'][2] in sharps and ne['sharp1']:
				sharps.append( ne['edgeids'][2] )

		sharp_count = len(sharps)
		
		if sharp_count == 0:
			
			print('SSMesh:split_normals, ERROR, how did you end up here????')
			return []
		
		elif sharp_count > 1:
			
			splits = []
			current_ne = None
			current_split = []
			
			i = 0
			# getting the first entry
			for ne in unique_pairs:
				if ne['edgeids'][0] == sharps[0]:
					current_ne = ne
					current_split.append( ne )
					i += 1
					break
			
			# pushing uniques sequentially, splitting at each sharp edge
			while i < len(unique_pairs):
				next_eid = current_ne['edgeids'][2]
				if next_eid in sharps:
					splits.append( current_split )
					current_split = []
				for ne in unique_pairs:
					if ne['edgeids'][0] == next_eid:
						current_ne = ne
						current_split.append( ne )
						i += 1
						break
			
			if len(current_split) > 0:
				splits.append( current_split )
			
			return splits
			
		else: #sharp_count == 1
			
			# most difficult case: no good way to resolve this, so let's split the
			# info in 2 groups starting at the sharp edge
			# getting the starting point
			sharpid = sharps[0]
			half = math.ceil( len(unique_pairs) / 2 )
			#print( '\tstarting at', k, ' 2 x', half )

			forward_arr = []
			backward_arr = []
			
			i = 0
			# seeking first forward entry
			next_forward = -1
			for ne in unique_pairs:
				if ne['edgeids'][0] == sharpid:
					next_forward = ne['edgeids'][2]
					forward_arr.append( ne )
					i += 1
					break
			while i < half:
				for ne in unique_pairs:
					if ne['edgeids'][0] == next_forward:
						next_forward = ne['edgeids'][2]
						forward_arr.append( ne )
						break
				i += 1
						
			# seeking first backward entry
			next_backwards = -1
			for ne in unique_pairs:
				if ne['edgeids'][2] == sharpid:
					next_backwards = ne['edgeids'][0]
					backward_arr.append( ne )
					i += 1
					break
			while i < len(unique_pairs):
				for ne in unique_pairs:
					if ne['edgeids'][2] == next_backwards:
						next_backwards = ne['edgeids'][0]
						backward_arr.append( ne )
						break
				i += 1
			
			return [forward_arr, backward_arr]
	
	def load(self, obj, do_dump):
		
		# just to be certain
		bpy.ops.object.mode_set(mode='OBJECT')
		bpy.ops.object.select_all(action='DESELECT')
		
		self.mesh = obj.data
		
		# getting all indices from group > require mode switch and therefore memory corruption
		self.tmp_groups = {}
		for gid in range(0,len(obj.vertex_groups)):
			self.get_group_ids(obj, gid)
			gid += 1
		
		# getting all vertex ids from faces (temporary)
		for p in self.mesh.polygons:
			if len(p.vertices) != 3:
				raise Exception('SSMesh, INVALID POLYGON! all faces must be triangle to be exported: ' + str(len(p.vertices)))
				return False
			for i in range(0,len(p.vertices)):
				self.indices.append(p.vertices[i])
		
		# getting all vertex ids from edges (temporary)
		for e in self.mesh.edges:
			for vref in e.vertices:
				if e.use_edge_sharp:
					if not vref in self.tmp_vert_sharp_count:
						self.tmp_vert_sharp_count[vref] = 1
					else:
						self.tmp_vert_sharp_count[vref] += 1
				self.tmp_edges.append(vref)
		
		self.last_vertid = 0
		self.last_anchorid = 0
		
		# identifying vertices type
		vid = 0
		for vert in self.mesh.vertices:
			self.identify_vertex(vid, vert)
			vid += 1
		
		# cleaning temp data
		self.indices = []
			
		# getting fibers and ligaments from analysis
		eid = 0
		for e in self.mesh.edges:
			v0 = self.tmp_verts[ e.vertices[0] ]
			v1 = self.tmp_verts[ e.vertices[1] ]
			if v0.ss_type == constants.SSType.SSANCHOR and v1.ss_type == constants.SSType.SSDOT:
				# pushing a ligament, anchor first
				self.ligaments.append(v0.ss_id)
				self.ligaments.append(v1.ss_id)
				self.tmp_ligaments.append(v0.blender_id)
				self.tmp_ligaments.append(v1.blender_id)
			elif v0.ss_type == constants.SSType.SSDOT and v1.ss_type == constants.SSType.SSANCHOR:
				# pushing a ligament, anchor first
				self.ligaments.append(v1.ss_id)
				self.ligaments.append(v0.ss_id)
				self.tmp_ligaments.append(v1.blender_id)
				self.tmp_ligaments.append(v0.blender_id)
			elif v0.ss_type == constants.SSType.SSDOT and v1.ss_type == constants.SSType.SSDOT:
				# pushing a fiber, no order required
				self.tmp_fibers_ids[ eid ] = len(self.fibers) / 2
				self.fibers.append(v0.ss_id)
				self.fibers.append(v1.ss_id)
				self.tmp_fibers.append(v0.blender_id)
				self.tmp_fibers.append(v1.blender_id)
			eid += 1
		
		# storing the easy stuff
		for vid in range(0,len(self.mesh.vertices)):
		
			v = self.tmp_verts[vid]
			if v.ss_type != constants.SSType.SSDOT and v.ss_type != constants.SSType.SSANCHOR:
				continue
			
			mat_loc = constants.SSMat @ mathutils.Matrix.Translation((v.vertex.co[0], v.vertex.co[1], v.vertex.co[2]))
			pos = mat_loc.to_translation()
			
			if v.ss_type == constants.SSType.SSDOT:
				self.dots.append(pos[0])
				self.dots.append(pos[1])
				self.dots.append(pos[2])
				
				mat_loc = constants.SSMat @ mathutils.Matrix.Translation((v.vertex.normal[0], v.vertex.normal[1], v.vertex.normal[2]))
				n = mat_loc.to_translation()
				self.normals.append(n[0])
				self.normals.append(n[1])
				self.normals.append(n[2])
				
			else:
				self.anchors.append(pos[0])
				self.anchors.append(pos[1])
				self.anchors.append(pos[2])
		
		# getting new index from analysis and face - edge (~fibers) relations
		for p in self.mesh.polygons:
			
			# from cull_front to cull_back, by default in godot engine
			for i in range(len(p.vertices)-1, -1, -1):
				# getting dots ids
				v = self.tmp_verts[ p.vertices[i] ]
				self.indices.append(v.ss_id)
				# getting normals
				self.load_norm_edges(p, v)
				# getting fibers
				bid = self.mesh.loops[ p.loop_start + i ].edge_index
				self.face_fibers.append(self.tmp_fibers_ids[ bid ])
		
		# merging normal infos: creating ultra simplified list out ot tmp_unique_edge_pair data
		# [ dotid, [bufferid, fiberid, flip, fiberid, flip], [bufferid, fiberid, flip, fiberid, flip], ...
		for k in self.tmp_unique_edge_pair:
			
			l = self.tmp_unique_edge_pair[k]
			
			if len(l) > 0 and not l[0]['merge']:
				splitted = self.split_normals( l )
				for uns in splitted:
					merged = []
					merged.append( k )
					for ne in uns:
						merged.append( ne['bufferid'] )
						merged += ne['edgeids']
					self.unique_normals.append( merged )
			else:
				merged = []
				merged.append( k )
				for ne in l:
					merged.append( ne['bufferid'] )
					merged += ne['edgeids']
				self.unique_normals.append( merged )
		
		# getting uvs
		for uvlayer in self.mesh.uv_layers:
			target = None
			if len(self.uvs) == 0:
				target = self.uvs
			elif len(self.uv2s) == 0:
				target = self.uv2s
			else:
				self.extra_uvs.append([])
				target = self.extra_uvs(len(self.extra_uvs)-1)
			for p in self.mesh.polygons:
				if len(p.vertices) != 3:
					continue
				# verifying it is a valid polygon
				for i in range(p.loop_total-1, -1, -1):
					uv = uvlayer.data[ p.loop_start + i ].uv
					target.append(uv.x)
					target.append(uv.y)
		
		# and now, the hardcore stuff: the groups
		gid = 0
		for g in obj.vertex_groups:
			
			#identifying group type:
			if g.name[:len(constants.SSH_GDOT_MARKER)] == constants.SSH_GDOT_MARKER:
				self.load_dot_group(obj, g, gid)
			
			elif g.name[:len(constants.SSH_GANCHOR_MARKER)] == constants.SSH_GANCHOR_MARKER:
				self.load_anchor_group(obj, g, gid)
			
			elif g.name[:len(constants.SSH_GFIBER_MARKER)] == constants.SSH_GFIBER_MARKER:
				self.load_fiber_group(obj, g, gid)
			
			elif g.name[:len(constants.SSH_GLIGAMENTS_MARKER)] == constants.SSH_GLIGAMENTS_MARKER:
				self.load_ligament_group(obj, g, gid)
			
			gid += 1
		
		if do_dump:
			self.dump()

		# trashing the memory
		self.tmp_edges = None
		self.tmp_verts = None
		self.tmp_fibers_ids = None
		self.tmp_fibers = None
		self.tmp_ligaments = None
		self.tmp_groups = None
		self.tmp_unique_edge_pair = None
		self.tmp_all_normal = None
		
		return True
	
	def serialise_string(self, file, s):
		txt = str(s)
		txt = txt[:constants.SSC_STRING_SIZE]
		while len(txt) < constants.SSC_STRING_SIZE:
			txt += chr(0)
		file.write(bytearray(txt, 'utf8'))
	
	def serialise_marker(self, file, s):
		txt = str(s)
		txt = txt[:constants.SSC_MARKER_SIZE]
		while len(txt) < constants.SSC_MARKER_SIZE:
			txt += chr(0)
		file.write(bytearray(txt, 'utf8'))
	
	def serialise_int(self, file, i):
		# using unsigned integer by default!
		# if you need to store negative numbers, use float
		file.write(struct.pack('I', int(i)))
		
	def serialise_float(self, file, i):
		file.write(struct.pack('f', float(i)))
	
	def serialise_floats(self, file, arr):
		self.serialise_int(file, len(arr))
		for v in arr:
			self.serialise_float(file, v)
		self.serialise_marker(file, constants.SSC_END_MARKER)
		file.flush()
		
	def serialise_ints(self, file, arr):
		self.serialise_int(file, len(arr))
		for v in arr:
			self.serialise_int(file, v)
		self.serialise_marker(file, constants.SSC_END_MARKER)
		file.flush()
	
	def save(self, path, generate_json):
		
		f = open(path, 'w+b')
		f.seek(0)
		
		self.serialise_string(f, constants.SSM_EXT)
		self.serialise_int(f, constants.SOFTSKIN_VERSION)
		
		self.serialise_marker(f, constants.SSC_SSMSTARTMA)
		self.serialise_floats(f, self.anchors)
		self.serialise_floats(f, self.dots)
		self.serialise_floats(f, self.normals)
		self.serialise_floats(f, self.tangents)
		self.serialise_floats(f, self.colors)
		self.serialise_floats(f, self.uvs)
		self.serialise_floats(f, self.uv2s)
		self.serialise_int(f, len(self.extra_uvs))
		for euv in self.extra_uvs:
			self.serialise_floats(f, euv)
		self.serialise_ints(f, self.indices)
		self.serialise_ints(f, self.face_fibers)
		self.serialise_ints(f, self.fibers)
		self.serialise_ints(f, self.ligaments)
		self.serialise_int(f, len(self.groups))
		for g in self.groups:
			self.serialise_string(f,g.name)
			self.serialise_int(f, g.ss_type)
			self.serialise_ints(f, g.indices)
			self.serialise_floats(f, g.weights)
		self.serialise_int(f, len(self.unique_normals))
		for ne in self.unique_normals:
			self.serialise_ints(f, ne)
		f.close()
		
		print( 'SSM saved at', path )
		
		# json generation
		if generate_json:
			f = open(path + '.json', 'w')
			jdict = {
				'anchors': self.anchors,
				'dots': self.dots,
				'normals': self.normals,
				'tangents': self.tangents,
				'colors': self.colors,
				'uvs': self.uvs,
				'uv2s': self.uv2s,
				'indices': self.indices,
				'face_fibers': self.face_fibers,
				'fibers': self.fibers,
				'ligaments': self.ligaments,
				'groups': [g.to_json() for g in self.groups],
				'unique_normals': [un for un in self.unique_normals]
			}
			json.dump(jdict, f)
			f.close()
		
			print( 'SSM json saved at', path + '.json' )
		
		return True