import bpy
import bmesh
import mathutils

bl_info = {
	"name": "Quad to Tri Cross",
	'author': "frankiezafe",
	'version': (1,0,0),
	'blender': (2, 74, 0),
	'location': "View3D > Mesh",
	'description': "Turn all quads of the mesh into a cross (4) of triangles",
	'warning': "",
	'wiki_url': "https://gitlab.com/frankiezafe/SoftSkin/wikis/home",
	'tracker_url': "https://gitlab.com/frankiezafe/SoftSkin/issues",
	'category': "Mesh"
}

class Quad2TriCrossOperator(bpy.types.Operator):
	
	bl_idname = "mesh.quad2tricross"
	bl_label = "Quad to Tri Cross"
	bl_options = {'REGISTER','UNDO'}
	
	def execute(self, context):
		
		obj =  bpy.context.active_object

		bpy.ops.object.mode_set(mode='EDIT')
		bpy.ops.mesh.select_all(action='DESELECT')

		bpy.ops.object.mode_set(mode='OBJECT')
		bpy.ops.object.select_all(action='DESELECT')

		def search_next_quad( mesh ):
			for p in mesh.polygons:
				if len(p.vertices) != 3:
					p.select = True
					vs = []
					for i in p.vertices:
						vs.append( i )
					return vs
			return None

		vs = search_next_quad(obj.data)

		while vs != None and len(vs) == 4:

			bpy.ops.object.mode_set(mode='EDIT')
			bpy.ops.mesh.delete(type='FACE')

			# barycenter of points
			bary = mathutils.Vector((0.0,0.0,0.0))
			for i in vs:
				bary += mathutils.Vector(obj.data.vertices[i].co)
			bary /= len( vs )

			bm = bmesh.from_edit_mesh(obj.data)
			bm.verts.new(bary)
			bmesh.update_edit_mesh(obj.data, False, True)

			#select new vert
			for i in range( 0, 4 ):
				nexti = (i + 1) % 4
				bpy.ops.mesh.select_all(action='DESELECT')
				bpy.ops.object.mode_set(mode='OBJECT')
				obj.data.vertices[ vs[i] ].select = True
				obj.data.vertices[ vs[nexti] ].select = True
				obj.data.vertices[ len( obj.data.vertices ) - 1 ].select = True
				bpy.ops.object.mode_set(mode='EDIT')
				bpy.ops.mesh.edge_face_add()

			bpy.ops.mesh.select_all(action='DESELECT')
			bpy.ops.object.mode_set(mode='OBJECT')	

			#vs = None
			vs = search_next_quad(obj.data)

		bpy.ops.object.mode_set(mode='EDIT')
		
		return {'FINISHED'}

def quad2tricross_button(self, context):
	self.layout.operator(Quad2TriCrossOperator.bl_idname)

def register():
	bpy.utils.register_class(Quad2TriCrossOperator)
	if __name__ != "__main__":
		bpy.types.VIEW3D_MT_edit_mesh_faces.append(quad2tricross_button)

def unregister():
	bpy.utils.unregister_class(Quad2TriCrossOperator)
	if __name__ != "__main__":
		bpy.types.VIEW3D_MT_edit_mesh_faces.remove(quad2tricross_button)

if __name__ == "__main__":
	register()
